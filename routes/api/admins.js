const mongoose = require("mongoose");
const passport = require("passport");
// const bodyParser = require("body-parser");
const router = require("express").Router();
// router.use(bodyParser.json());
const auth = require("../auth");
const Admins = mongoose.model("Admins");
const Publics = mongoose.model("Publics");
const Beds = mongoose.model("Beds");

router.post("/", auth.optional, (req, res, next) => {
  const {
    body: { user },
  } = req;

  if (!user) return res.status(422).json({ errors: "user object is required" });

  if (!user.email) return res.status(422).json({ errors: "mail is required" });

  if (!user.password) return res.status(422).json({ errors: "Password is required" });

  const finalUser = new Admins(user);

  finalUser.setPassword(user.password);

  finalUser.save((err) => {
    if (err) return res.status(422).json({ error: "save failed" });
    return res.json({ user: finalUser.toAuthJSON() });
  });
});

router.post("/login", auth.optional, (req, res, next) => {
  const {
    body: { user },
  } = req;

  if (!user.email) return res.status(422).json({ errors: "Email is required" });

  if (!user.password) return res.status(422).json({ errors: "Password is required" });

  return passport.authenticate("admin", { session: false }, (err, passportUser, info) => {
    if (err) return res.status(422).json({ errors: "Wrong log" });

    if (passportUser) {
      const user = passportUser;
      user.token = passportUser.generateJWT();

      return res.json({ user: user.toAuthJSON() });
    }

    return status(400).info;
  })(req, res, next);
});

router.get("/read", auth.required, (req, res, next) => {
  const {
    payload: { id },
  } = req;

  return Admins.findById(id).then((user) => {
    if (!user) return res.sendStatus(422).json({ error: "Admin not found" });
    return res.json({ user: user.toAuthJSON() });
  });
});

router.get("/read-public", auth.required, (req, res) => {
  const {
    body: { idSearch },
  } = req;

  return Publics.findById(idSearch).then((user) => {
    if (!user) return res.sendStatus(422).json({ error: "User not found" });
    return res.json({ user: user.toAuthJSON() });
  });
});

router.get("/read-all-publics", auth.required, (req, res, next) => {
  return Publics.find()
    .populate("bed")
    .then((allPublics) => {
      if (!allPublics) return res.sendStatus(422).json({ error: "Users not found" });
      return res.json(allPublics.map((public) => public.toAuthJSON()));
    });
});

router.put("/update-public-bed", auth.required, (req, res, next) => {
  const {
    body: { idUpdate, bed },
  } = req;

  if (bed) {
    Beds.findByIdAndUpdate(bed, { available: false }, { new: true }).then(() => {
      Publics.findOneAndUpdate({ bed: bed }, { bed: null }).then(() => {
        Publics.findByIdAndUpdate(idUpdate, { bed: bed }, { new: true }, (err, result) => {
          if (err) return res.json({ error: "error" });
          return res.json({ user: result.toAuthJSON() });
        });
      });
    });
  }
  if (!bed) {
    Publics.findById(idUpdate).then((result) => {
      Beds.findByIdAndUpdate(result.bed, { available: true }).then(() => {
        Publics.findByIdAndUpdate(idUpdate, { bed: null }).then((result) => {
          if (err) return res.json({ error: "error" });
          return res.json({ user: result.toAuthJSON() });
        });
      });
    });
  }
});

router.delete("/delete-admin", auth.required, (req, res) => {
  const {
    payload: { id },
  } = req;
  const {
    body: { id_remove },
  } = req;

  if (id === id_remove) {
    return res.json({ message: "You can't remove your actual account" });
  } else {
    Admins.findByIdAndRemove(id_remove, (err, result) => {
      if (err) return res.sendStatus(422).json({ error: "Remove failed" });
      return res.json({ message: `${result.email} removed` });
    });
  }
});

router.delete("/delete-public", auth.required, (req, res) => {
  const {
    body: { id_remove },
  } = req;

  Publics.findByIdAndRemove(id_remove, (err, result) => {
    if (err) return res.sendStatus(422).json({ error: "Remove failed" });
    return res.json({ message: `${result.email} removed` });
  });
});

router.post("/search-free-bed-per-stage", auth.required, (req, res) => {
  const {
    body: { stage },
  } = req;

  Beds.find({ stage }, (err, result) => {
    if (err) return res.sendStatus(422).json({ error: "No beds" });
    return res.json({ results: result.map((resultBed) => resultBed) });
  });
});

router.post("/search-bed-per-id", auth.required, (req, res) => {
  const {
    body: { id },
  } = req;

  Publics.findById(id)
    .populate("bed")
    .then((result) => {
      return res.json({ result: { stage: result.bed.stage, number: result.bed.number } });
    });
});

module.exports = router;
