
const express = require('express');
const router = express.Router();

router.use('/admins', require('./admins'));
router.use('/publics', require('./publics'))
router.use('/beds', require('./beds'))

module.exports = router;