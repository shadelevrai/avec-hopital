const mongoose = require("mongoose");
const router = require("express").Router();
const auth = require("../auth");
const Beds = mongoose.model("Beds");

router.get("/", auth.required, (req, res) => {
  
  Beds.find({}, (err, result) => {
    if (err) return res.status(422).json({ errors: "beds don't find" });
    return res.json({ result });
  });
});

router.post("/create", auth.required, (req, res) => {
  const {
    body: { bed },
  } = req;

  const finalBed = new Beds(bed);

  finalBed.save((err) => {
    if (err) return res.status(422).json({ errors: "bed don't create" });
    return res.json({ bed: finalBed.toAuthJSON() });
  });
});

router.put("/update", auth.required, (req, res) => {
  const {
    body: { id, bed },
  } = req;
  Beds.findByIdAndUpdate(id, bed, { new: true }, (err, result) => {
    if (err) return res.sendStatus(422).json({ errors: "update failed" });
    return res.json({ bed: result.toAuthJSON() });
  });
});

router.delete("/remove", auth.required, (req, res) => {
  const {
    body: { id },
  } = req;

  Beds.findByIdAndRemove(id, (err, result) => {
    if (err) return res.status(422).json({ errors: "Remove failed" });
    return res.json({ message: `${result._id} removed` });
  });
});

module.exports = router;
