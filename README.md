## Getting Started

First, install dependency

```
npm i
```

If you use a mongoDB in local, don't forget to launch your mongo server (mongod)

Change the .env file in the server folder

Exemple : 
PORT=8887
MONGO_CONNECT=mongodb://localhost:27017/myapp

To launch the project : 
```
npm run dev
```

# API LIST

## Except for create and login a admin / user public, you need a valid Token from the server.

## Admins

> /api/admins/

Create an admin

#### expected object :
```
{
    user : {
        email : String,
        password : String
    }
}
```

> /api/admins/login

Login an admin

#### expected object :
```
{
    user : {
        email : String,
        password : String
    }
}
```

> /api/admins/read

Read the admin info

```
{

}
```

> /api/admins/read-public

Read a public user

#### expected object :
```
{
    idSearch : String
}
```

> /api/admins/read-all-publics

Read all public user

#### expected object :
```
{
    
}
```

> /api/admins/update-public-bed

Update a public bed

#### expected object :
```
{
    idUpdate : String,
    bed : String
}
```

> /api/admins/delete-admin

Delete an admin

#### expected object :
```
{
    idRemove : String
}
```

> /api/admins/delete-public

Delete a public user

#### expected object :
```
{
    idRemove : String
}
```

> /api/admins/search-free-bed-per-stage

Search a free bed per stage

#### expected object :
```
{
    stage : String
}
```

> /api/admins/search-bed-per-id

Search a bed info per id

#### expected object :
```
{
    id : String
}
```


## Publics

> /api/publics/

Create a public user

#### expected object :
```
{
    user : {
        email : String,
        password : String
    }
}
```

> /api/publics/login

Login a public user

#### expected object :
```
{
    user : {
        email : String,
        password : String
    }
}
```

> /api/publics/read

Read a public user

#### expected object :
```
{

}
```

## Beds

> /api/beds/

Read a bed

#### expected object :
```
{

}
```

> /api/beds/create

Create a bed

#### expected object :
```
{
    bed : {
        number : Number,
        Stage : Number,
        available : Boolean,
        occupation :  {
            begin : Date,
            ending : Date
        }
    }
}
```

> /api/beds/update

Update a bed

#### expected object :
```
{
    id : String,
    bed : {
        number : Number,
        Stage : Number,
        available : Boolean,
        occupation :  {
            begin : Date,
            ending : Date
        }
    }
}
```

> /api/beds/remove

remove a bed

#### expected object :
```
{
    id : String,
}
```


## NODENV

https://github.com/dkundel/node-env-run