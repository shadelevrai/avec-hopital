const mongoose = require("mongoose");
const passport = require("passport");
const LocalStrategy = require("passport-local");

const Admins = mongoose.model("Admins");
const Publics = mongoose.model("Publics");

passport.use('admin',
  new LocalStrategy(
    {
      usernameField: "user[email]",
      passwordField: "user[password]",
    },
    (email, password, done) => {
      Admins.findOne({ email })
        .then((user) => {
          if (!user || !user.validatePassword(password)) {
            return done(null, false, { errors: { "email or password": "is invalid" } });
          }

          return done(null, user);
        })
        .catch(done);
    }
  )
);

passport.use('public',
  new LocalStrategy(
    {
      usernameField: "user[email]",
      passwordField: "user[password]",
    },
    (email, password, done) => {
      Publics.findOne({ email })
        .then((user) => {
          if (!user || !user.validatePassword(password)) {
            return done(null, false, { errors: { "email or password": "is invalid" } });
          }

          return done(null, user);
        })
        .catch(done);
    }
  )
);