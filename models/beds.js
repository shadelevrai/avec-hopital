const mongoose = require("mongoose");

const { Schema } = mongoose;

const BedsSchema = new Schema({
  number: { type: Number, required: true },
  stage: { type: Number, required: true },
  available: { type: Boolean, default: true },
  occupation: {
    begin: { type: Date, default: null },
    end: { type: Date, default: null },
  },
});

BedsSchema.methods.toAuthJSON = function () {
  return {
    _id: this._id,
    number: this.number,
    stage: this.stage,
    available: this.available,
    occupation: this.occupation,
  };
};

mongoose.model("Beds", BedsSchema);
