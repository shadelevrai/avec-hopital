const mongoose = require("mongoose");
const crypto = require("crypto");
const jwt = require("jsonwebtoken");

const { Schema } = mongoose;

const PublicsSchema = new Schema({
  email: String,
  bed: {
    type: mongoose.Schema.Types.ObjectID || Object,
    ref: 'Beds',
    default:null
  },
  hash: String,
  salt: String,
});

PublicsSchema.methods.setPassword = function (password) {
  this.salt = crypto.randomBytes(16).toString("hex");
  this.hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, "sha512").toString("hex");
};

PublicsSchema.methods.validatePassword = function (password) {
  const hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, "sha512").toString("hex");
  return this.hash === hash;
};

PublicsSchema.methods.generateJWT = function () {
  const today = new Date();
  const expirationDate = new Date(today); 
  expirationDate.setDate(today.getDate() + 60);

  return jwt.sign(
    {
      email: this.email,
      id: this._id,
      exp: parseInt(expirationDate.getTime() / 1000, 10),
    },
    "secret"
  );
};

PublicsSchema.methods.toAuthJSON = function () {
  return {
    _id: this._id,
    email: this.email,
    bed:this.bed,
    token: this.generateJWT(),
  };
};

mongoose.model("Publics", PublicsSchema);
