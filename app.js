require('dotenv').config()

const express = require('express');
const app = express();
const cors = require("cors")
const path = require('path');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const session = require('express-session');

// app.use(bodyParser.urlencoded({
//     extended: false
// }));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({
    secret: 'passport-tutorial',
    cookie: {
        maxAge: 60000
    },
    resave: false,
    saveUninitialized: false
}));
app.use(cors())

mongoose.connect(process.env.MONGO_CONNECT, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
});
mongoose.set('debug', true);

require('./models/Admins');
require('./models/Publics');
require('./models/Beds');
require('./config/passport');


app.use(require('./routes'))

app.listen(process.env.PORT, () => console.log(`Server running on http://localhost:${process.env.PORT}/`));